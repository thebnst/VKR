const faqData = [
    { question: "Как забронировать номер?", answer: "Забронировать номер можно по телефону +7 (123) 456-78-90 или на сайте." },
    { question: "Какие услуги включены в стоимость проживания?", answer: "В стоимость проживания включены завтрак, обед и ужин, а также использование бассейна и спортивных площадок." },
    { question: "Как добраться до санатория?", answer: "Санаторий расположен в 10 км от города, до которого ходят рейсы автобусов и поездов. Также можно добраться на такси или арендованном автомобиле." },
    { question: "Какой возрастной категории детей предназначен детский санаторий?", answer: "Детский санаторий предназначен для детей от 3 до 14 лет." },
    { question: "Какие возможности для отдыха есть в детском санатории?", answer: "У нас есть разнообразные развлекательные и культурные мероприятия, игровые комнаты, кинозал, библиотека, а также экскурсии в окрестности." },
    { question: "Какие услуги предоставляются в детском санатории?", answer: " Мы предоставляем комплексное лечение и реабилитацию, а также оздоровительные программы для детей. Это включает в себя лечение заболеваний, физиотерапию, массаж, бассейн, спортивные занятия и многое другое." }
];

  
  const faqList = document.querySelector(".faq-list");
  
  faqData.forEach(faq => {
    const faqItem = document.createElement("div");
    faqItem.classList.add("faq-item");
  
    const faqQuestion = document.createElement("h3");
    faqQuestion.classList.add("faq-question");
    faqQuestion.textContent = faq.question;
    faqQuestion.addEventListener("click", () => {
      faqAnswer.classList.toggle("active");
    });
  
    const faqAnswer = document.createElement("div");
    faqAnswer.classList.add("faq-answer");
    faqAnswer.textContent = faq.answer;
  
    faqItem.appendChild(faqQuestion);
    faqItem.appendChild(faqAnswer);
    faqList.appendChild(faqItem);
  });
  