const form = document.querySelector('form');
const responseDiv = document.querySelector('.response');

form.addEventListener('submit', function(event) {
  event.preventDefault();
  const name = document.getElementById('name').value;
  const phone = document.getElementById('phone').value;
  const message = document.getElementById('message').value;
  const operatorResponse = document.querySelector('.operator-response');
  operatorResponse.innerHTML = `Спасибо, ${name}! Мы свяжемся с вами в ближайшее время.`;
  responseDiv.style.display = 'block';
  form.reset();
});
