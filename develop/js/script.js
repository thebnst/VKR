
// Кнопки/Всплывающие окна
const btn1 = document.getElementById('btn1');
const btn2 = document.getElementById('btn2');
const btn3 = document.getElementById('btn3');
const btn4 = document.getElementById('btn4');
const btn5 = document.getElementById('btn5');
const btn6 = document.getElementById('btn6');
const modalContainer1 = document.getElementById('modal-container1');
const modalContainer2 = document.getElementById('modal-container2');
const modalContainer3 = document.getElementById('modal-container3');
const modalContainer4 = document.getElementById('modal-container4');
const modalContainer5 = document.getElementById('modal-container5');
const modalContainer6 = document.getElementById('modal-container6');
const closeBtn1 = document.getElementById('close-modal1');
const closeBtn2 = document.getElementById('close-modal2');
const closeBtn3 = document.getElementById('close-modal3');
const closeBtn4 = document.getElementById('close-modal4');
const closeBtn5 = document.getElementById('close-modal5');
const closeBtn6 = document.getElementById('close-modal6');

btn1.addEventListener('click', () => {
	modalContainer1.style.display = 'block';
});

btn2.addEventListener('click', () => {
	modalContainer2.style.display = 'block';
});

btn3.addEventListener('click', () => {
	modalContainer3.style.display = 'block';
});

btn4.addEventListener('click', () => {
	modalContainer4.style.display = 'block';
});

btn5.addEventListener('click', () => {
	modalContainer5.style.display = 'block';
});

btn6.addEventListener('click', () => {
	modalContainer6.style.display = 'block';
});

closeBtn1.addEventListener('click', () => {
	modalContainer1.style.display = 'none';
});

closeBtn2.addEventListener('click', () => {
	modalContainer2.style.display = 'none';
});

closeBtn3.addEventListener('click', () => {
	modalContainer3.style.display = 'none';
});

closeBtn4.addEventListener('click', () => {
	modalContainer4.style.display = 'none';
});

closeBtn5.addEventListener('click', () => {
	modalContainer5.style.display = 'none';
});

closeBtn6.addEventListener('click', () => {
	modalContainer6.style.display = 'none';
});

// Галерея



