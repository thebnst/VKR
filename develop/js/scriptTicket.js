const popupBtns = document.querySelectorAll('.popup-btn');
const popups = document.querySelectorAll('.popup-overlay');

popupBtns.forEach(function(btn) {
  btn.addEventListener('click', function() {
    const popupId = btn.getAttribute('data-popup');
    const popup = document.querySelector(popupId);

    popup.style.display = 'flex';

    popup.querySelector('.popup-close').addEventListener('click', function() {
      popup.style.display = 'none';
    });

    popup.addEventListener('click', function(e) {
      if (e.target === this) {
        popup.style.display = 'none';
      }
    });
  });
});
