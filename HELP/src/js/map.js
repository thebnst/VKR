// Подключение библиотеки Leaflet
const L = require('leaflet');

// Инициализация карты
const map = L.map('map').setView([51.505, -0.09], 13); // Здесь указывается начальное местоположение и уровень масштабирования

// Добавление плитки карты (например, OpenStreetMap)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// Добавление маркера на карту
const marker = L.marker([51.5, -0.09]).addTo(map); // Здесь указывается местоположение маркера

// Добавление всплывающей подсказки к маркеру
marker.bindPopup("<b>Мы здесь!</b>").openPopup();
