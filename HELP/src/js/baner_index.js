// Функция для анимации баннеров
function animateBanners() {
    const bannerContainer = document.querySelector(".banner-container");
    const banners = document.querySelectorAll(".banner");

    let currentIndex = 0;

    setInterval(() => {
        // Смещение баннера влево
        currentIndex++;
        if (currentIndex >= banners.length) {
            currentIndex = 0;
        }

        // Изменение стиля для анимации
        const offset = -currentIndex * 300; // 300 - ширина баннера
        bannerContainer.style.transform = `translateX(${offset}px)`;
        }, 3000); // Интервал в миллисекундах (например, 3 секунды)
}

// Вызов функции для анимации баннеров после загрузки страницы
window.addEventListener("load", animateBanners);
