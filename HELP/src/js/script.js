// Скрываем блок регистрации при загрузке страницы
document.getElementById("register-container").style.display = "none";

// Показываем блок регистрации при клике на "Зарегистрироваться"
document.getElementById("show-register").addEventListener("click", function(event) {
    event.preventDefault();
    document.getElementById("login-container").style.display = "none";
    document.getElementById("register-container").style.display = "block";
});

// Показываем блок входа при клике на "Войти"
document.getElementById("show-login").addEventListener("click", function(event) {
    event.preventDefault();
    document.getElementById("register-container").style.display = "none";
    document.getElementById("login-container").style.display = "block";
});

// Обработчик формы входа
document.getElementById("login-form").addEventListener("submit", function(event) {
    event.preventDefault();
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    // Здесь можно добавить код для проверки логина и пароля
    // Например, отправить запрос на сервер для аутентификации

    // После успешной аутентификации можно перенаправить пользователя на другую страницу
});

// Обработчик формы регистрации
document.getElementById("register-form").addEventListener("submit", function(event) {
    event.preventDefault();
    const newUsername = document.getElementById("new-username").value;
    const newPassword = document.getElementById("new-password").value;

    // Здесь можно добавить код для регистрации нового пользователя
    // Например, отправить запрос на сервер для создания новой учетной записи
});




