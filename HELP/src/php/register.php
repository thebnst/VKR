<?php

    // mysql -u php_chech -p
    // USE mydb;
    // SELECT * FROM users;
    // INSERT INTO users (login, pass) VALUES ('user1', 'password1');
    // GRANT ALL PRIVILEGES ON my_data_bass.* TO 'php_chech'@'localhost';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Подключение к базе данных
    $servername = "localhost";
    $username = "php_chech";
    $password = "root";
    $dbname = "mydb";

    // Создание соединения с базой данных
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Проверка соединения
    if ($conn->connect_error) {
        die("Ошибка подключения: " . $conn->connect_error);
    }

    $newUsername = $_POST["new-username"];
    $newPassword = $_POST["new-password"];

    // Проверка, существует ли пользователь с таким именем
    $checkUserQuery = "SELECT * FROM users WHERE login = ?";
    $checkUserStatement = $conn->prepare($checkUserQuery);
    $checkUserStatement->bind_param("s", $newUsername);
    $checkUserStatement->execute();
    $result = $checkUserStatement->get_result();

    if ($result->num_rows > 0) {
        echo "Пользователь с таким именем уже существует. Пожалуйста, выберите другое имя пользователя.";
    } else {
        // Хеширование пароля
        $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

        // Вставка нового пользователя в базу данных
        $insertUserQuery = "INSERT INTO users (login, pass) VALUES (?, ?)";
        $insertUserStatement = $conn->prepare($insertUserQuery);
        $insertUserStatement->bind_param("ss", $newUsername, $hashedPassword);

        if ($insertUserStatement->execute()) {
            echo "Регистрация прошла успешно. Теперь вы можете войти.";
        } else {
            echo "Ошибка при регистрации: " . $conn->error;
        }
    }

    // Закрываем соединение с базой данных
    $conn->close();
}
?>
