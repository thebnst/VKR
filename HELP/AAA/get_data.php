<?php
// Подключение к базе данных
$servername = "localhost";
$username = "your_username";
$password = "your_password";
$dbname = "your_database";

// Создание соединения
$conn = new mysqli($servername, $username, $password, $dbname);

// Проверка соединения
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// SQL запрос для выборки данных из таблицы
$sql = "SELECT * FROM your_table";
$result = $conn->query($sql);

// Формирование HTML кода для отображения данных
if ($result->num_rows > 0) {
    echo "<table>";
    echo "<tr><th>ID</th><th>Column1</th><th>Column2</th></tr>";
    // Вывод данных каждой строки таблицы
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["id"]. "</td><td>" . $row["column1"]. "</td><td>" . $row["column2"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 результатов";
}

// Закрытие соединения с базой данных
$conn->close();
?>
